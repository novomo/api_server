const { RedisPubSub } = require("graphql-redis-subscriptions");
const { REDIS_PASS, REDIS_PORT, REDIS_HOSTNAME } = require("../env");

const options = {
  host: REDIS_HOSTNAME,
  port: REDIS_PORT,
  password: REDIS_PASS,
  retry_strategy: (options) => {
    // reconnect after
    return Math.max(options.attempt * 100, 3000);
  },
};

const redisPubSub = new RedisPubSub({
  connection: options,
});

module.exports.pubsub = redisPubSub;
