const { ApolloServer } = require("@apollo/server");
const DataLoader = require("dataloader");
// Apollo server
// typeDefs: Graphql type definitions
// resolvers: How to resolve the queries
const context = require("./context");
const path = require("path");
const { loadFilesSync } = require("@graphql-tools/load-files");
const { mergeTypeDefs, mergeResolvers } = require("@graphql-tools/merge");
const { makeExecutableSchema } = require("@graphql-tools/schema");
const { WebSocketServer } = require("ws");
const { useServer } = require("graphql-ws/lib/use/ws");
const is_auth = require("./server_middleware/auth");
const {
  ApolloServerPluginDrainHttpServer,
} = require("@apollo/server/plugin/drainHttpServer");
const { app } = require("./express");
const https = require("https");
const fs = require("fs");
const { CERT_KEY, CERT_PEM } = require("./env");
const typesArray = loadFilesSync(`${__dirname}/graphql/**/*.graphql`);
const typeDefs = mergeTypeDefs(typesArray);

const resolversArray = loadFilesSync(
  path.join(__dirname, "./graphql/**/*.resolver.*")
);

const resolvers = mergeResolvers(resolversArray);

const schema = makeExecutableSchema({ typeDefs, resolvers });

const httpsServer = https
  .createServer(
    {
      key: fs.readFileSync(CERT_KEY, "utf8"),
      cert: fs.readFileSync(CERT_PEM, "utf8"),
    },
    app
  )
  .addListener("session", (session) => {
    session.setTimeout(60_000, () => session.destroy(new Error("TIMEOUT")));
  })
  .addListener("stream", (stream, headers) => {
    stream.addListener("error", (err) => stream.destroy(err));
  })
  .addListener("clientError", function (err, socket) {
    socket.destroy(err);
    /* On client connection error */
  })
  .addListener("sessionError", (err, socket) => {
    socket.destroy(err);
  })
  .on("secureConnection", function (socket) {
    socket.on("error", (err) => {
      console.log(err);
    });
    /* On client connection error */
  })
  .setTimeout(120_000);

// ws Server
const wsServer = new WebSocketServer({
  server: httpsServer,
  path: "/graphql", // localhost:3000/graphql
});

const serverCleanup = useServer({ schema }, wsServer); // dispose

const server = new ApolloServer({
  introspection: true,
  playground: true,
  schema,

  plugins: [
    ApolloServerPluginDrainHttpServer({ httpServer: httpsServer }),
    {
      async serverWillStart() {
        return {
          async drainServer() {
            await serverCleanup.dispose();
          },
        };
      },
    },
  ],
  context: context,
});

module.exports.server = server;
module.exports.httpsServer = httpsServer;
