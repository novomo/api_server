#!/usr/bin/node
// imported
const { server, httpsServer } = require("./apollo_server");
const { initDB } = require("./data/mysql/sql_connection");
const { httpServer, httpApp, app } = require("./express");
const { expressMiddleware } = require("@apollo/server/express4");
const { HOSTNAME, PORT } = require("./env");
//setupObserver();
const cluster = require("cluster");
const os = require("os");
const cors = require("cors");
const context = require("./context");
const numCPUs = os.cpus().length;
const IP = require("ip");
const upload_error = require("./node_error_functions/upload_error");

(async () => {
  try {
    initDB();
    if (cluster.isPrimary) {
      //console.log(`Primary ${process.pid} is running`);

      // Fork workers.
      for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
      }

      cluster.on("exit", (worker, code, signal) => {
        //console.log(`worker ${worker.process.pid} died`);
      });
    } else {
      // Workers can share any TCP connection
      // In this case it is an HTTP server

      await server.start();
      console.log("starting server");
      httpApp.use(
        "/graphql",
        cors({
          origin: true,
          credentials: true,
        }),
        expressMiddleware(server, {
          context: context,
        })
      );
      app.use(
        "/graphql",
        cors({
          origin: true,
          credentials: true,
        }),
        expressMiddleware(server, {
          context: context,
        })
      );
      httpServer.listen(httpApp.get("port"), function () {
        console.log(
          "Express HTTP server listening on port " + httpApp.get("port")
        );
      });
      httpsServer.listen({ port: PORT || 3000 }, () =>
        console.log("🚀 Server ready at", `https://${HOSTNAME}:${PORT}`)
      );

      //console.log(`Worker ${process.pid} started`);
    }
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Starting API",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical,
    });
  }
})();
/*
server.listen({ port: PORT || 3000 }, () =>
  //console.log("🚀 Server ready at", `https://${HOSTNAME}:${PORT}`)
);
*/
