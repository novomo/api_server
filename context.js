const {
  getHorseByIds,
} = require("./graphql/sportevents_graphql/dataloaders/horse_dataloader");
const {
  getBetByIds,
} = require("./graphql/bets_graphql/dataloaders/bet_dataloader");
const {
  getEsportsEventByIds,
} = require("./graphql/sportevents_graphql/dataloaders/esportsevent_dataloader");
const {
  getSportsEventByIds,
} = require("./graphql/sportevents_graphql/dataloaders/sportsevent_dataloader");
const {
  getGreyhoundRaceByIds,
} = require("./graphql/sportevents_graphql/dataloaders/greyhoundrace_dataloader");
const {
  getHorseRaceByIds,
} = require("./graphql/sportevents_graphql/dataloaders/horserace_dataloader");
const {
  getTeamByIds,
} = require("./graphql/sportevents_graphql/dataloaders/team_dataloader");
const {
  getCompetitionByIds,
} = require("./graphql/sportevents_graphql/dataloaders/competition_dataloader");
const {
  getJockeyByIds,
} = require("./graphql/sportevents_graphql/dataloaders/jockey_dataloader");
const {
  getTrainerByIds,
} = require("./graphql/sportevents_graphql/dataloaders/trainer_dataloader");
const {
  getCourseByIds,
} = require("./graphql/sportevents_graphql/dataloaders/horsetrack_dataloader");
const {
  getGreyhoundByIds,
} = require("./graphql/sportevents_graphql/dataloaders/greyhound_dataloader");
const {
  getGreyhoundTrackByIds,
} = require("./graphql/sportevents_graphql/dataloaders/greyhoundtrack_dataloader");
const {
  getSportsTradeByIds,
} = require("./graphql/sports_trading_graphql/dataloaders/sportstrade_dataloader");
const {
  getTipsterByIds,
} = require("./graphql/tips_graphql/dataloaders/tipster_dataloader");
//const { setupObserver } = require("./src/server/observer");
const is_auth = require("./server_middleware/auth");
//setupObserver();
const IP = require('ip');
const upload_error = require("./node_error_functions/upload_error")
const DataLoader = require("dataloader");

module.exports = async ({ req, res }) => {
  try {
  let currentUser = null;
  let permissions = null;
  let token = null;
  try {
    console.log(req.headers);
    const authToken = req.headers["authorization"];
    console.log(authToken);
    if (authToken) {
      [token, currentUser, permissions] = await is_auth(authToken);
    }
    console.log(token, currentUser, permissions);
  } catch (e) {
    // Doesn't through error cause there are so public resolvers that can be reached.
    console.log(e);
    return {
      betDataLoader: new DataLoader(getBetByIds),
      horseDataLoader: new DataLoader(getHorseByIds),
      horseRaceDataLoader: new DataLoader(getHorseRaceByIds),
      eSportEventDataLoader: new DataLoader(getEsportsEventByIds),
      sportEventDataLoader: new DataLoader(getSportsEventByIds),
      greyhoundRaceDataLoader: new DataLoader(getGreyhoundRaceByIds),
      teamDataLoader: new DataLoader(getTeamByIds),
      competitionDataLoader: new DataLoader(getCompetitionByIds),
      jockeyDataLoader: new DataLoader(getJockeyByIds),
      trainerDataLoader: new DataLoader(getTrainerByIds),

      horseRaceTrackDataLoader: new DataLoader(getHorseRaceTackByIds),

      greyhoundDataLoader: new DataLoader(getGreyhoundByIds),

      greyhoundTrackDataLoader: new DataLoader(getGreyhoundTrackByIds),

      sportsTradeDataLoader: new DataLoader(getSportsTradeByIds),

      tipsterDataLoader: new DataLoader(getTipsterByIds),
      req,
      res,
      token,
      currentUser,
      permissions,
    };
  }
  // returns all needed user data if it is found
  console.log(token, currentUser, permissions);
  return {
    betDataLoader: new DataLoader(getBetByIds),
    horseDataLoader: new DataLoader(getHorseByIds),
    horseRaceDataLoader: new DataLoader(getHorseRaceByIds),
    eSportEventDataLoader: new DataLoader(getEsportsEventByIds),
    sportEventDataLoader: new DataLoader(getSportsEventByIds),
    greyhoundRaceDataLoader: new DataLoader(getGreyhoundRaceByIds),
    teamDataLoader: new DataLoader(getTeamByIds),
    competitionDataLoader: new DataLoader(getCompetitionByIds),
    jockeyDataLoader: new DataLoader(getJockeyByIds),
    trainerDataLoader: new DataLoader(getTrainerByIds),

    horseRaceTrackDataLoader: new DataLoader(getCourseByIds),

    greyhoundDataLoader: new DataLoader(getGreyhoundByIds),

    greyhoundTrackDataLoader: new DataLoader(getGreyhoundTrackByIds),

    sportsTradeDataLoader: new DataLoader(getSportsTradeByIds),

    tipsterDataLoader: new DataLoader(getTipsterByIds),
    req,
    res,
    token,
    currentUser,
    permissions,
  };
} catch(err) {
  console.log(err)
    upload_error({
      errorTitle: "Creating API context",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true
    })
}
};
