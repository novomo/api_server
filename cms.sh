(git pull)
(git submodule update --init --recursive)
input="$PWD/.gitmodules"
while IFS= read -r line
do
  if [[ "$line" == *"path"* ]]; then
    IFS=' ' read -ra ADDR <<< "$line"
    
    echo "${ADDR[2]}"

    (cd "$PWD/${ADDR[2]}" && git checkout main)
    (cd "$PWD/${ADDR[2]}" && rm yarn.lock)
    (cd "$PWD/${ADDR[2]}" && git pull)
    (cd "$PWD/${ADDR[2]}" && yarn install)

  fi
done < "$input"