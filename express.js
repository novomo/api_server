/*
    Express App Configuration
*/
// node mmodules
const express = require("express");
const cookieParser = require("cookie-parser");
const { verify } = require("jsonwebtoken");
const cors = require("cors");
const http = require("http");
const httpApp = express();
const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("./env.js");
let { client, reconnect } = require("./data/mysql/sql_connection");

httpApp.set("port", 80);
httpApp.get("*", function (req, res, next) {
  res.redirect("https://" + req.headers.host + req.path);
});
httpApp.post("*", function (req, res, next) {
  res.redirect("https://" + req.headers.host + req.path);
});
httpServer = http.createServer(httpApp);

// user defined imports
const {
  createAccessToken,
  createRefreshToken,
  sendRefreshToken,
  sendExtensionToken,
} = require("./server_middleware/tokens");

// initiate express app
const app = express();

// add middleware to app
app.use(cookieParser());
app.enable("trust proxy");
app.use(
  cors({
    origin: true,
    credentials: true,
  })
);

app.use(express.json({ limit: "50mb" }));
app.use(function (request, response, next) {
  if (!request.secure) {
    return response.redirect("https://" + request.headers.host + request.url);
  }

  next();
});

app.use(function (req, res, next) {
  res.setTimeout(360000, function () {
    //console.log("Request has timed out.");
    res.sendStatus(408);
  });

  next();
});

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header(
    "Access-Control-Allow-Methods",
    "GET,PUT,POST,DELETE,UPDATE,OPTIONS"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept"
  );
  next();
});

app.post("/refresh_token", async (req, res) => {
  const token = req.cookies.jid;
  ////console.log("requested token")
  if (!token) {
    return res.send({ ok: false, accessToken: "no token" });
  }

  let payload = null;
  try {
    payload = verify(token, process.env.RJWT_KEY);
    ////console.log(payload)
  } catch (err) {
    //console.log(err);
    return res.send({ ok: false, accessToken: "not verified" });
  }

  // token is valid and
  // we can send back an access token
  ////console.log(payload)
  const user = await client.getRow(
    `SELECT * FROM users where userID = ${payload.userId};`
  );

  if (!user) {
    return res.send({ ok: false, accessToken: "no user" });
  }

  if (user.tokenVersion !== payload.tokenVersion) {
    return res.send({ ok: false, accessToken: "wrong tokenVersion" });
  }

  sendRefreshToken(res, createRefreshToken(user));
  return res.send({ ok: true, accessToken: createAccessToken(user) });
});

module.exports.app = app;
module.exports.httpServer = httpServer;
module.exports.httpApp = httpApp;
